# Tutorial: Creating a simple clicker game in Castle Game Engine and Castle Editor

This tutorial is aimed at new users, who have some minor knowledge of Pascal and general concepts of programming and game development. It will cover all steps necessary from creation and setting up of the project, to a small but finished game.

## Ascii Doctor

The tutorial text is written in Ascii Doctor syntax, see https://asciidoctor.org/ for details.

## Feedback

If you have any questions, suggestions or other feedback, please use Issues in this repository: https://github.com/eugeneloza/castle-editor-tutorial/. You can also use Castle Game Engine official Discord server or official forum, links to which you can find at: https://castle-engine.io/talk.php.
