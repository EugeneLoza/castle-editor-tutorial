# BUTTON CLICKER GAME

A clicker game requiring speed, accuracy and concentration.

This game was created as a tutorial for Castle Game Engine (https://castle-engine.io/). The tutorial text and source code will be available in several days.

## About the game

In this game you have to click the buttons on screen.

Each button has a growing score - clicking the button adds this score to your total score.

However, if the button grows to score 999 - it's Game Over!

Each button starts without a score - avoid clicking such buttons, as it will increase the game pace.

Get as many points as you can and earn achievements!

Good luck and have fun!

## Installation

### Windows

No installation required - just extract the game into one folder and play.

### Linux

No installation required - just extract the game into one folder and play. Most likely you'll also need to set "executable" flag for the binary (`chmod +x ButtonClickerGame` or through file properties in the file manager).

You need the following libraries installed to play the game:

* libopenal1
* libpng
* zlib1g
* libvorbis
* libfreetype6
* You also need X-system, GTK at least version 2 and OpenGL drivers for your videocard, however, you are very likely to have those already installed :)

### Android

WARNING: Android version is still Beta, there are at least 2 known non-critical bugs.

Download and install the APK.

Game requires permission to use device vibration.

## Credits

Created in Castle Game Engine (https://castle-engine.io/).

Featuring music by Alexandr Zhelanov (https://soundcloud.com/alexandr-zhelanov) and Matthew Pablo (http://www.matthewpablo.com).

Sounds by johnthewizar, Kenney, newagesoup and unfa.

Fonts by HolyBlackCat and Karen B. Jones.

Graphics by pzUH and Charles Rondeau.
